from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy_print, diplomacy_eval

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):

	# ----
  # read
  # ----

	def test_read_1(self):
		s = "A Madrid Hold\n"
		i = diplomacy_read(s)
		self.assertEqual(i, [["A","Madrid","Hold"]])

	def test_read_2(self):
		s = "A Madrid Hold\nB London Support A\nC Austin Move London\n"
		i = diplomacy_read(s)
		self.assertEqual(i, [["A","Madrid","Hold"],["B","London","Support","A"],["C","Austin","Move","London"]])

	def test_read_3(self):
		s = "A Madrid Hold\nB Barcelona Support A\n"
		i = diplomacy_read(s)
		self.assertEqual(i, [["A","Madrid","Hold"],["B","Barcelona","Support","A"]])

	# ----
  # eval
  # ----

	def test_eval_1(self):
		i = [["A","Madrid","Hold"]]
		t = [["A","Madrid"]]
		r = diplomacy_eval(i)
		self.assertEqual(t,r)

	def test_eval_2(self):
		i = [["A","Madrid","Hold"],["B","Barcelona","Move","Madrid"]]
		t = [["A","[dead]"],["B","[dead]"]]
		r = diplomacy_eval(i)
		self.assertEqual(t,r)

	def test_eval_3(self):
		i = [["A","Madrid","Hold"],["B","Barcelona","Move","Madrid"],["C","London","Support","B"]]
		t = [["A","[dead]"],["B","Madrid"],["C","London"]]
		r = diplomacy_eval(i)
		self.assertEqual(t,r)
        
	def test_eval_4(self):
		i = [["A","Madrid","Hold"],["B","Barcelona","Move","Madrid"],["C","London","Support","B"],["D","Austin","Move","London"]]
		t = [["A","[dead]"],["B","[dead]"],["C","[dead]"],["D","[dead]"]]
		r = diplomacy_eval(i)
		self.assertEqual(t,r)
        
	def test_eval_5(self):
		i = [["A","Madrid","Hold"],["B","Barcelona","Move","Madrid"],["C","London","Move","Madrid"],["D","Paris","Support","B"]]
		t = [["A","[dead]"],["B","Madrid"],["C","[dead]"],["D","Paris"]]
		r = diplomacy_eval(i)
		self.assertEqual(t,r)

	# ----
  # print
  # ----
        
	def test_print_1(self):
		w = StringIO()
		diplomacy_print([["A", "Madrid"]], w)
		self.assertEqual(w.getvalue(), "A Madrid\n")

	def test_print_2(self):
		w = StringIO()
		diplomacy_print([["B", "[dead]"]], w)
		self.assertEqual(w.getvalue(), "B [dead]\n")

	def test_print_3(self):
		w = StringIO()
		diplomacy_print([["A", "Madrid"],["B", "London"]], w)
		self.assertEqual(w.getvalue(), "A Madrid\nB London\n")

	# ----
  # solve
  # ----
	
	def test_solve_1(self):
		r = StringIO("A Madrid Hold\n")
		w = StringIO()
		diplomacy_solve(r,w)
		self.assertEqual(w.getvalue(), "A Madrid\n")

	def test_solve_2(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n D Austin Move London\n")
		w = StringIO()
		diplomacy_solve(r,w)
		self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

	def test_solve_3(self):
		r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
		w = StringIO()
		diplomacy_solve(r,w)
		self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestDiplomacy.out



% cat TestDiplomacy.out
..............
----------------------------------------------------------------------
Ran 14 tests in 0.003s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          71      3     60      4    95%   81, 86, 94, 69->64, 80->81, 85->86, 91->94
TestDiplomacy.py      70      0      0      0   100%
--------------------------------------------------------------
TOTAL                141      3     60      4    97%
"""